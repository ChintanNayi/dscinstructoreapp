import { whileStatement } from '@babel/types';
import React, { Component } from 'react'
import {
    StyleSheet, View, Image, Button, Text,
    TouchableOpacity,
    ScrollView
} from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import TruckParking from '../assets/img/truck_parking.png'
import LOGO from '../assets/img/dsc-main-logo.png'
import Ripple from 'react-native-material-ripple';

export class Welocomscreen extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} />
                    </View>
                    <Image style={styles.mainImg} source={TruckParking} />
                    <Ripple rippleColor="#99FF99" rippleOpacity={0.9} onPress={() => this.props.navigation.navigate('Login')} style={styles.loginBtn}>
                    <Text style={styles.loginText}>SIGN IN</Text>
                </Ripple>

                <View style={styles.bottom}>
                    <Text style={styles.textbottom}>@ 2021 Driving School Cloud. All rights reserved</Text>
                </View>
            </View>
            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('10%'),
        marginTop: (Platform.OS === 'ios') ? 50 : 20,
    },
    mainImg: {
        width: wp('90%'),
        height: hp('40%'),
        marginTop: (Platform.OS === 'ios') ? 60 : 60
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: (Platform.OS === 'ios') ? 30 : 30,
        // marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    textbottom: {
        height: wp('10%'),
        color: 'grey',
        // marginTop: 50,
        
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginTop:(Platform.OS === 'ios') ? 160 : 150,
      }
})

export default Welocomscreen



