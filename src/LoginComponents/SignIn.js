import React, { Component } from 'react'
import { Button, View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import axios from 'axios';
import { SafeAreaView } from 'react-native-safe-area-context';
import LOGO from '../assets/img/dsc-main-logo.png'
import Ripple from 'react-native-material-ripple';

export class SignIn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: ''
        }
    }

    SignIn = () => {
        const nav = this.props.navigation
        const ldata = { email: this.state.email, password: this.state.password }
        const headers = {
            'Content-Type': 'multipart/form-data'
        }

        nav.navigate('SendOTP');

        // axios
        //     .post('https://vibrantsofttech.com/project/prosourceadvisory/api/authentication/login', ldata, {
        //         headers: headers
        //     })
        //     .then(function (response) {
        //         // handle success
        //         console.log("res", response.data.status)

        //         if (response.data.status === true) {
        //             nav.navigate('SendOTP');
        //             console.log("ifff")
        //         }
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         alert(error.message);
        //     });
    }

    render() {
        return (
            <ScrollView>
                <SafeAreaView style={styles.container}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} />
                    </View>

                    <Text style={styles.head}>Sign In</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ email: text })}
                        placeholder='User Name'
                        placeholderTextColor='grey'
                        underlineColorAndroid="transparent"
                        value={this.state.email}
                    />

                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ password: text })}
                        placeholder='Password'
                        placeholderTextColor='grey'
                        underlineColorAndroid="transparent"
                        value={this.state.password}
                        secureTextEntry={true}
                    />
                    <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{flex:1, justifyContent: 'flex-end'}}  onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                        <Text style={styles.forgot_button}>Forgot Password?</Text>
                    </TouchableOpacity>
                      </View>
                    <Ripple rippleColor="#99FF99" rippleOpacity={0.9} onPress={() => this.SignIn()} style={styles.loginBtn}>
                        <Text style={styles.loginText}>SIGN IN</Text>
                    </Ripple>

                </SafeAreaView>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: hp('10%'),
        marginTop:100
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 30,
        paddingBottom:30,
        fontSize: hp('4%'),
        color: '#4d4d4d'
    },
    title: {
        height: hp('4%'),
        marginBottom: 20,
        textAlign: 'center',
        color: 'grey',
        marginTop: 20
    },
    input: {
        height: hp('7%'),
        width: wp('80%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
        margin: 10,
        color:'black'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: 45,
    },
    forgot_button: {
        textAlign: 'right',
        flex: 1,
        marginRight: 40,
        marginTop: 20,
        color:'black'
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default SignIn
