import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LOGO from '../assets/img/dsc-main-logo.png';
import Ripple from 'react-native-material-ripple';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { color } from 'react-native-reanimated';

var { Platform } = React;

export class EnterOTP extends Component {
    constructor(props) {
        super(props)

        this.state = {
            timer: null,
            counter: 59,
            phone: '+919924460329',
            confirmResult: null,
            verificationCode: '',
            userId: ''
        }

    }

    componentDidMount() {
        this.startTimer();
    }

    handleVerifyCode = () => {
        // Request for OTP verification
        const nav = this.props.navigation
        nav.navigate('ScreenExternal');
    }

    startTimer = () => {
        this.setState({
            show: false
        })
        let timer = setInterval(this.manageTimer, 1000);
        this.setState({ timer });

    }

    manageTimer = () => {

        var states = this.state

        if (states.counter === 0) {
            clearInterval(this.state.timer)
            this.setState({
                counter: 59,
                show: true
            })
        }
        else {
            this.setState({
                counter: this.state.counter - 1
            });

        }
    }

    componentWillUnmount() {
        clearInterval(this.state.timer);
    }


    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} />
                    </View>
                    <Text style={styles.head}>Get Your One-Time Code</Text>
                    <OTPInputView
                        autoFocusOnLoad
                        placeholderCharacter='0'
                        placeholderTextColor='grey'
                        style={{width: '70%', height: 50}}
                        pinCount={6}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    />
                    <Ripple rippleColor="#99FF99" rippleOpacity={0.9} onPress={() => this.props.navigation.navigate('ScreenExternal')} style={styles.loginBtn}>
                        <Text style={styles.loginText}>CONTINUE</Text>
                    </Ripple>

                    {this.state.show === true ?

                        <View style={styles.timerView}>
                            <Text style={{ textAlign: 'center', marginRight: 10, color: 'grey' }}>00:00</Text>
                            <TouchableOpacity onPress={() => this.startTimer()}>
                                <Text style={styles.resend}>Resend Code</Text>
                            </TouchableOpacity>

                        </View> :
                        <View style={styles.timerView}>
                            <Text style={{ textAlign: 'center', marginRight: 10, color: 'grey' }}>00:{this.state.counter}</Text>
                            <Text style={styles.resend1}>Resend Code</Text>
                        </View>

                    }
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop:100
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 70,
        fontSize: 25,
        color: '#4d4d4d'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: (Platform.OS === 'ios') ? hp('9%') : hp('10%'),
        marginTop: 45
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    inputGroup: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40
    },
    resend: {
        textAlign: 'center',
        color: '#00a64f'
    },
    resend1: {
        textAlign: 'center',
        color: 'grey'
    },
    underlineStyleBase: 
    {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 3,
        color:'black'
    },
    underlineStyleHighLighted: 
    {
        borderColor: 'black',
        borderBottomColor: "#50ad50",   
    },
    timerView:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom:20,
    }
});

export default EnterOTP
