import React, { Component } from 'react'
import {
    SafeAreaView,
    ScrollView,
    Switch,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    Modal,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CalendarStrip from 'react-native-calendar-strip'
import { Card, Avatar, ListItem, Input, Button, ThemeProvider } from 'react-native-elements'

const theme = {
    Button: {
        titleStyle: {
            color: 'white',
        },
    },
};

export default class CourseList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedDate: '',
            date: new Date(),
            course: [
                {
                    name: 'John Smit',
                    id: '#23415',
                    endtime: '12pm',
                    count: '6',
                    timer: '01h00mm00s',
                },
                {
                    name: 'John Smit',
                    id: '#23415',
                    endtime: '11pm',
                    count: '3',
                    timer: '01h00mm00s',
                },
                {
                    name: 'John Smit',
                    id: '#23415',
                    endtime: '10am',
                    count: '4',
                    timer: '01h00mm00s',
                },
                {
                    name: 'John Smit',
                    id: '#23415',
                    endtime: '2pm',
                    count: '6',
                    timer: '01h00mm00s',
                }

            ],
            start: false,
            id: '',
            modalVisible: false,
            isModalVisible: false

        }
        this.handleSwitchChange = this.handleSwitchChange.bind(this);
    }

    handleSwitchChange = (i) => {
        this.setState({
            start: !this.state.start,
            id: i
        })
    }

    render() {
        return (
            <SafeAreaView>
                <View style={styles.container}>
                    {/* <View style={styles.head}>
                        <Image style={styles.image} source={Menu} />
                        <Text style={styles.headText}>View TimeSheet Course 1 (Theory)</Text>
                    </View> */}
                    <View style={styles.listView}>
                        <Text style={styles.headtext}>View Timesheet Course 1 (Theory)</Text>
                    </View>
                    <CalendarStrip
                        scrollable
                        calendarAnimation={{ type: 'sequence', duration: 30 }}
                        daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#00a64f' }}
                        style={styles.celendarCom}
                        calendarHeaderStyle={{ color: 'black' }}
                        calendarColor={'#7743CE'}
                        dateNumberStyle={{ color: 'black' }}
                        dateNameStyle={{ color: 'black' }}
                        highlightDateNumberStyle={{ color: '#00a64f' }}
                        highlightDateNameStyle={{ color: '#00a64f' }}
                        disabledDateNameStyle={{ color: '#00a64f' }}
                        disabledDateNumberStyle={{ color: '#00a64f' }}
                        iconContainer={{ flex: 0.1 }}
                        selectedDate={this.state.date}
                        onDateSelected={date => this.setState({ selectedDate: date })}
                    />

                    <ScrollView>
                        {
                            this.state.course.map((data, i) => {
                                return (

                                    <Card key={i}>
                                        <View style={styles.cardContent}>
                                            <View style={{ alignItems: 'center', flexDirection: 'row', width: wp('35%') }}>
                                                <TouchableOpacity onPress={() => {
                                                    this.setState({ modalVisible: !this.state.modalVisible });
                                                }}>
                                                    <Avatar
                                                        size="small"
                                                        rounded
                                                        source={{ uri: 'https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg' }}
                                                        activeOpacity={0.7}
                                                    />
                                                </TouchableOpacity>
                                                <ListItem.Content>
                                                    <ListItem.Title style={{ fontSize: 15,color:'black' }}>{data.name}</ListItem.Title>
                                                    <ListItem.Subtitle style={{ fontSize: 13, color: '#606060' }}>{data.id}</ListItem.Subtitle>
                                                </ListItem.Content>
                                            </View>
                                            <View>
                                                <View style={{ alignItems: 'center', flexDirection: 'row', marginRight: 35, marginTop: 8 }}>
                                                    <Text style={{color:'#606060', fontSize: 12}}>Start Time:</Text>
                                                    <Text style={{ color: '#606060',fontSize: 12, paddingLeft: 5 }}>11pm</Text>
                                                </View>
                                                <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                                    <Text style={{color:'#606060', fontSize: 12}}>End Time:</Text>
                                                    <Text style={{ color: '#606060',fontSize: 12, paddingLeft: 5 }}>1pm</Text>
                                                </View>
                                            </View>
                                            <View style={{ alignItems: 'center',marginRight: 15, marginTop: 10 }}>
                                                <Text style={{color:'black', fontSize: 10, fontWeight: 'bold'}}>Attendance</Text>
                                                {this.state.id === i ? <Switch onChange={() => this.handleSwitchChange(i)} style={{ marginTop: 5 }} value={this.state.start} color="#00a64f" />
                                                    : <Switch onChange={() => this.handleSwitchChange(i)} style={{ marginTop: 5 }} value={false} color="#00a64f" />
                                                }
                                            </View>
                                        </View>
                                    </Card>

                                )
                            })
                        }
                    </ScrollView>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            // this.closeButtonFunction()
                            this.setState({ modalVisible: !this.state.modalVisible });
                        }}>
                        <View style={styles.viewWrapper}>

                            <View style={styles.modalView}>
                                <View style={{ alignItems: 'center' }}>
                                    <Avatar
                                        size="xlarge"
                                        rounded
                                        source={{
                                            uri:
                                                'https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg',
                                        }}
                                    />
                                </View>

                                <Text style={{ bottom: 30, fontWeight: 'bold', alignSelf: 'center',color:'black' }}>Joe Smith</Text>
                                <Text style={{ bottom: 30, color: 'grey', alignSelf: 'center' }}>#1234567</Text>
                                <Text style={{ left: 10, bottom: 10, fontWeight: 'bold' }}>Class Name Lorem Ipsum</Text>
                                <Input
                                    label='Add Student FeedBack'
                                    placeholder='Enter Add Student FeedBack'
                                />
                                <Input
                                    label='Private Note each Student'
                                    placeholder='Enter Private Note'
                                />
                                {/** This button is responsible to close the modal */}
                                <ThemeProvider theme={theme}>
                                    <View style={{ width: wp('90%'), marginLeft: 20 }}>
                                        <Button
                                            title="Save"
                                            buttonStyle={{ backgroundColor: '#00a64f', }}
                                            style={{ marginBottom: 20, }}
                                            onPress={() => {
                                                this.setState({ modalVisible: !this.state.modalVisible });
                                            }}
                                        />
                                    </View>
                                </ThemeProvider>
                            </View>
                        </View>
                    </Modal>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // alignItems: 'center',
    },
    head: {
        backgroundColor: '#1c1c1c',
        flexDirection: 'row',
        textAlign: 'center'
    },
    image: {
        width: wp('10%'),
        height: hp('5%'),
        marginLeft: 20,
    },
    headText: {
        color: 'white',
        marginTop: 10,
        marginLeft: 40,
        fontWeight: 'bold',
        fontSize: 15
    },
    listView: {
        alignContent: 'center',
        alignItems: 'center'
    },
    headtext: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        color:'#00a64f',
        marginTop:20,
        marginBottom:10
    },
    cardTitle1: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: 90,
        paddingBottom: 10
    },
    cardTitle2: {
        flexDirection: 'column',
    },
    sView: {
        flexDirection: 'row',
        marginLeft: 1,
    },
    cardContent: {
        flexDirection: 'row',
        textAlign: 'center',
        justifyContent: 'space-between'
    },
    viewWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
    modalView: {
        position: "absolute",
        elevation: 5,
        top: '35%',
        backgroundColor: "#fff",
        borderRadius: 7,
        width: wp('100%'),
        height: hp('70%')
    },
    textInput: {
        width: wp('5%'),
        marginBottom: 8,
    },
    celendarCom: {
        height: hp('10%'),
        width: wp('90%'),
        backgroundColor: 'white',
        marginLeft: 20,
        marginTop: 10,
        padding: 5
    },
    textColor: {
        color: 'black'
    },
});
