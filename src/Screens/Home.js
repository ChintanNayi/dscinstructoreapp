import React, { Component } from 'react'
import {
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Text,
  Alert,
  View,
  Image,
  Modal,
  TextInput,
  PermissionsAndroid
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Menu from '../assets/img/menu.png';
import CalendarStrip from 'react-native-calendar-strip'
import { Card, Button, Input, ThemeProvider } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const theme = {
  Button: {
    titleStyle: {
      color: 'red',
    },
  },
};

export default class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      
      selectedDate: '',
      date: new Date(),

      course: [
        {
          courseName: '1',
          stime: '11pm',
          endtime: '12pm',
          count: '6',
          timer: '01h00mm00s',
        },
        {
          courseName: '4',
          stime: '10pm',
          endtime: '11pm',
          count: '3',
          timer: '01h00mm00s',
        },

      ],
      start: false,
      id: '',
      modalVisible: false,
      isModalVisible: false

    }
    
    this.handleSwitchChange = this.handleSwitchChange.bind(this);
    this.showConfirmDialog = this.showConfirmDialog.bind(this);
    this.takePhotoFromCamera=this.takePhotoFromCamera.bind(this);
  }

  takePhotoFromCamera = async () => {ImagePicker.openCamera({
    width: 300,
    height: 400,
    cropping: true,
  }).then(image => {
    console.log(image);
  });
  }

  handleSwitchChange = (i) => {

    if (this.state.start === false) {
      this.showConfirmDialog(i);
    } else {
      this.setState({
        isModalVisible: true,
        start: !this.state.start,
        id: i
      })
    }

  }

  showConfirmDialog = (i) => {
    return Alert.alert(
      "Are Your Sure?",
      "Are you sure you want to start class?",
      [
        // The "Yes" button
        {
          text: "Start",
          onPress: () => {
            this.setState({
              start: !this.state.start,
              id: i,
            })
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: "Cancel",
        },
      ]
    );
  };

  toggleModalVisibility = () => {
    this.setState({
      isModalVisible: false
    })
  };

  render() {

    return (
      <SafeAreaView>
        <View style={styles.container}>
        <View style={styles.listView}>
            <Text style={styles.headtext}>Timesheet</Text>
          </View>
          <CalendarStrip
              scrollable
              calendarAnimation={{ type: 'sequence', duration: 30 }}
              daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#00a64f' }}
              style={styles.celendarCom}
              calendarHeaderStyle={{ color: 'black', paddingTop: 10, fontSize: 15 }}
              calendarColor={'#7743CE'}
              dateNumberStyle={{ color: 'black' }}
              dateNameStyle={{ color: 'black' }}
              highlightDateNumberStyle={{ color: '#00a64f' }}
              highlightDateNameStyle={{ color: '#00a64f' }}
              disabledDateNameStyle={{ color: '#00a64f' }}
              disabledDateNumberStyle={{ color: '#00a64f' }}
              iconContainer={{ flex: 0.1 }}
              selectedDate={this.state.date}
              onDateSelected={date => this.setState({ selectedDate: date })}
          />
          <View style={styles.listView}>
            <Text style={styles.headtext}>Today Class List</Text>
          </View>
          <ScrollView>
            {
              this.state.course.map((data, i) => {
                return (

                  <Card key={i}>
                   
                      <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10}}>
                        <View style={styles.cardTitle1}>
                          <Text style={{ color: '#00a64f' }}>Course {data.courseName}</Text><Text style={{ color: '#606060', marginLeft: 5 }}>(Theory)</Text>
                        </View>
                        <View style={styles.cardTitle2}>
                          <View style={styles.sView}>
                            <Text style={{ color: '#00a64f' }}>Start Time:</Text>
                            <Text style={{ color: '#00a64f', marginLeft: 5 }}>{data.stime}</Text>
                          </View>
                          <View style={styles.sView}>
                            <Text style={{ color: '#00a64f', marginTop: 5 }}>End Time:</Text>
                            <Text style={{ color: '#00a64f', marginLeft: 13, marginTop: 5 }}>{data.endtime}</Text>
                          </View>
                        </View>
                      </View>
                   
                    <Card.Divider />
                    <View style={styles.cardContent}>
                      <View style={{ alignItems: 'center' }}>
                        <Text style={styles.textColor}>Student Count</Text>
                        <Text style={styles.colorGreen}>{data.count}</Text>
                      </View>
                      <View style={{ alignItems: 'center' }}>
                        <Text style={styles.textColor}>Timer</Text>
                        <Text style={styles.colorGreen}>{data.timer}</Text>
                      </View>
                      <View style={{ alignItems: 'center' }}>
                        <Text style={styles.textColor}>Start Classes</Text>
                        {this.state.id === i ? <Switch onChange={() => this.handleSwitchChange(i)} style={{ marginTop: 5 }} value={this.state.start} color="#00a64f" />
                          : <Switch onChange={() => this.handleSwitchChange(i)} style={{ marginTop: 5 }} value={false} color="#00a64f" />
                        }
                      </View>
                    </View>
                  </Card>

                )
              })
            }
          </ScrollView>
          {/* <Modal animationType="slide"
            transparent visible={this.state.isModalVisible}
            presentationStyle="overFullScreen"
            onDismiss={this.toggleModalVisibility}> */}
            {/* <View style={styles.viewWrapper}>
              <View style={styles.modalView}>

              <View style={{flexDirection: 'row'}}>
          
                <Input
                  style={styles.textInput}
                  label='Start Miles'
                  placeholder='Enter Start Miles'
                />
                 <Icon 
                  style={styles.icons}
                  name="camera" 
                  size={30}
                  onPress={this.takePhotoFromCamera}
                />
                
              </View> */}

                {/** This button is responsible to close the modal */}
                {/* <View style={{ flexDirection: 'row' }}>
                  <ThemeProvider theme={theme}>
                    <Button
                      title="Cancel"
                      titleStyle={{ color: '#00a64f' }}
                      buttonStyle={{ borderColor: '#00a64f', borderRadius: 20 }}
                      style={{ width: wp('30%') }}
                      type="outline"
                      onPress={this.toggleModalVisibility}
                    />
                    <Button
                      title="Save"
                      titleStyle={{ color: 'white' }}
                      buttonStyle={{ backgroundColor: '#00a64f', borderRadius: 20 }}
                      style={{ width: wp('30%'), marginLeft: 20 }}
                      onPress={this.toggleModalVisibility}
                    />
                  </ThemeProvider>
                </View>
                
              </View>
            </View> */}
          {/* </Modal> */}
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // alignItems: 'center',
  },
  head: {
    backgroundColor: '#1c1c1c',
    flexDirection: 'row',
    textAlign: 'center'
  },
  image: {
    width: wp('10%'),
    height: hp('5%'),
    marginLeft: 20,
  },
  headText: {
    color: 'black',
    marginTop: 10,
    marginLeft: 90
  },
  listView: {
    alignContent: 'center',
    alignItems: 'center'
  },
  headtext: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#00a64f',
    marginTop:10
  },
  cardTitle1: {
    flexDirection: 'row',
    marginTop:10
  },
  cardTitle2: {
    flexDirection: 'column',
  },
  sView: {
    flexDirection: 'row',
    marginLeft: 1,
  },
  cardContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    textAlign: 'center'
  },
  viewWrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.2)",
  },
  modalView: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: "40%",
    left: "5%",
    elevation: 5,
    backgroundColor: "#fff",
    borderRadius: 7,
    width: wp('90%'),
    height: hp('30%')
  },
  icons: {
    marginTop: 10,
  },
  textInput: {
    width: wp('5%'),
    marginBottom: 8,
    marginLeft: 15,
  },
  celendarCom:{
    height: hp('10%'),
    width: wp('90%'), 
    backgroundColor: 'white',
    marginLeft:20,
    marginTop:10,
  },
  textColor:{
    color:'black'
  },
  colorGreen:{
    color: '#00a64f', 
    paddingTop: 5
  }

});
