import React, { Component } from 'react'


import { Alert, LayoutAnimation, StyleSheet, View, Text, ScrollView, UIManager, TouchableOpacity, Platform, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Menu from '../assets/img/menu.png';
import CalendarStrip from 'react-native-calendar-strip'
import { Avatar, ListItem } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';

export default class StudentList extends Component {

    constructor() {

        super();

        this.state = {
            list: [
                {
                    name: 'Amy Farha',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                    subtitle: '#123456'
                },
                {
                    name: 'Chris Jackson',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
                    subtitle: '#1234567'
                },
            ],
            show: false,
            ready: false,
            id: ''
        }
    }

    showList = (i) => {
        this.setState({
            show: !this.state.show,
            id: i
        })
    }

    render() {
        return (
            <SafeAreaView>
                {/* <View style={styles.head}>
                    <Image style={styles.image} source={Menu} />
                    <Text style={styles.headText}>Dashoboard</Text>
                </View> */}
                <View>
                    <View style={{ flexDirection: 'column'}}>
                        {
                            this.state.list.map((l, i) => (
                                <View key={i} style={{marginTop:10}}>
                                    <TouchableOpacity onPress={() => this.showList(i)} key={i} >
                                        <ListItem bottomDivider>
                                            {this.state.ready === true ?
                                                <View>
                                                    {this.state.id === i ?
                                                        <View style={styles.leftReadyView}></View> :
                                                        <View style={styles.leftNotReadyView}></View>
                                                    }
                                                </View>
                                                : <View style={styles.leftNotReadyView}></View>}
                                            <Avatar
                                                size="small"    
                                                rounded
                                                source={{ uri: l.avatar_url }}
                                                activeOpacity={0.7}
                                            />
                                            <ListItem.Content>
                                                <ListItem.Title style={{ fontSize: 15,color:'black' }}>{l.name}</ListItem.Title>
                                                <ListItem.Subtitle style={{ fontSize: 13, color: 'grey' }}>{l.subtitle}</ListItem.Subtitle>
                                            </ListItem.Content>
                                            {this.state.ready === true ?
                                                <View>
                                                    {this.state.id === i ?
                                                        <Text style={{ textAlign: 'center', color: '#00a64f' }}>Ready To Test</Text> :
                                                        null
                                                    }
                                                </View>
                                                : null}
                                            <Image
                                                source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
                                                style={styles.iconStyle} />
                                        </ListItem>
                                    </TouchableOpacity>
                                    {this.state.show === true ?
                                        <View>
                                            {this.state.id === i ?
                                                <View key={i} style={{ flexDirection: 'column', marginLeft: 5 }}>
                                                    <TouchableOpacity onPress={() => { this.setState({ ready: !this.state.ready, id: i }) }}>
                                                        <ListItem bottomDivider>
                                                            <View style={styles.readyBtn}><Text style={styles.btnText}>Ready To Test</Text></View>
                                                        </ListItem>
                                                    </TouchableOpacity>
                                                    <ListItem bottomDivider>
                                                        <ListItem.Content style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <ListItem.Title>Course 1 (Theory)</ListItem.Title>
                                                            <ListItem.Title>
                                                                <View style={styles.cardTitle2}>
                                                                    <View style={styles.sView}>
                                                                        <Text>Start Time:</Text>
                                                                        <Text style={{ marginLeft: 5 }}>11pm</Text>
                                                                    </View>
                                                                    <View style={styles.sView}>
                                                                        <Text style={{ marginTop: 5 }}>End Time:</Text>
                                                                        <Text style={{ marginLeft: 13, marginTop: 5 }}>12pm</Text>
                                                                    </View>
                                                                </View>
                                                            </ListItem.Title>
                                                        </ListItem.Content>
                                                    </ListItem>
                                                    <ListItem bottomDivider>
                                                        <ListItem.Content style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <ListItem.Title>Course 1 (Theory)</ListItem.Title>
                                                            <ListItem.Title>
                                                                <View style={styles.cardTitle2}>
                                                                    <View style={styles.sView}>
                                                                        <Text>Start Time:</Text>
                                                                        <Text style={{ marginLeft: 5 }}>11pm</Text>
                                                                    </View>
                                                                    <View style={styles.sView}>
                                                                        <Text style={{ marginTop: 5 }}>End Time:</Text>
                                                                        <Text style={{ marginLeft: 13, marginTop: 5 }}>12pm</Text>
                                                                    </View>
                                                                </View>
                                                            </ListItem.Title>
                                                        </ListItem.Content>
                                                    </ListItem>
                                                    <ListItem bottomDivider>
                                                        <ListItem.Content style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <ListItem.Title>Course 1 (Theory)</ListItem.Title>
                                                            <ListItem.Title>
                                                                <View style={styles.cardTitle2}>
                                                                    <View style={styles.sView}>
                                                                        <Text>Start Time:</Text>
                                                                        <Text style={{ marginLeft: 5 }}>11pm</Text>
                                                                    </View>
                                                                    <View style={styles.sView}>
                                                                        <Text style={{ marginTop: 5 }}>End Time:</Text>
                                                                        <Text style={{ marginLeft: 13, marginTop: 5 }}>12pm</Text>
                                                                    </View>
                                                                </View>
                                                            </ListItem.Title>
                                                        </ListItem.Content>
                                                    </ListItem>
                                                </View> : null}
                                        </View>
                                        : null}
                                </View>
                            ))
                        }
                    </View>
                </View>
            </SafeAreaView >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // alignItems: 'center',
    },
    head: {
        backgroundColor: '#1c1c1c',
        flexDirection: 'row',
        textAlign: 'center'
    },
    image: {
        width: wp('10%'),
        height: hp('5%'),
        marginLeft: 20,
    },
    headText: {
        color: 'white',
        marginTop: 10,
        marginLeft: 90,
        fontSize: 20
    },
    iconStyle: {
        width: 30,
        height: 30,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    sub_Category_Text: {
        fontSize: 18,
        color: '#000',
        padding: 10
    },
    category_Text: {
        textAlign: 'left',
        fontSize: 21,
        padding: 10
    },
    category_View: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    cardTitle2: {
        flexDirection: 'column',
    },
    sView: {
        flexDirection: 'row',
        marginLeft: 1,
    },
    readyBtn: {
        marginLeft: 120,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#00a64f',
        width: wp('30%'),
        height: hp('3%'),
        textAlign: 'center',
        backgroundColor: '#00a64f',
    },
    btnText: {
        color: 'white',
        textAlign: 'center',
        paddingTop: 0
    },
    leftReadyView: {
        backgroundColor: '#00a64f',
        height: hp('6%'),
        width: wp('3%'),
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    leftNotReadyView: {
        backgroundColor: '#feba24',
        height: hp('6%'),
        width: wp('3%'),
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    }

});
