import React from 'react';
import { View, StyleSheet } from 'react-native';
import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Drawer,
    Text,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import Dashboard from '../Screens/Home';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import { TouchableOpacity } from 'react-native-gesture-handler';

// import{ AuthContext } from '../components/context';

export function DrawerContent(props) {

    const paperTheme = useTheme();

    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <TouchableOpacity 
                    style={{margin: 20}}
                    onPress={props.navigation.closeDrawer}>
                <Icon 
                    name='close'
                    color={'white'}
                    size={25}
                />
                </TouchableOpacity>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection:'row'}}>
                            <Avatar.Image 
                                source={{
                                    uri: 'https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg'
                                }}
                                size={90}
                            />
                            <View style={{marginLeft:15, flexDirection:'column', justifyContent: 'center'}}>
                                <Title style={styles.title}>John Doe</Title>
                                {/* <Caption style={styles.caption}>@j_doe</Caption> */}
                            </View>
                        </View>
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name='text-snippet'
                                color={'white'}
                                size={size}
                                />
                            )}
                            label={ () => ( <Text style={{color: 'white'}}>Timesheet</Text>) }
                            onPress={() => {props.navigation.navigate(Dashboard)}}
                        />
                       <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name='groups'
                                color={'white'}
                                size={size}
                                />
                            )}
                            label={ () => ( <Text style={{color: 'white'}}>Student List</Text>) }
                            onPress={() => {props.navigation.navigate('Student List')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name='person'
                                color={'white'}
                                size={size}
                                />
                            )}
                            label={ () => ( <Text style={{color: 'white'}}>Edit Profile</Text>) }
                            onPress={() => {props.navigation.navigate('Edit Profile')}}
                        />
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem 
                    icon={({size}) => (
                        <Icon 
                        name="exit-to-app" 
                        color= {'white'}
                        size={size}
                        />
                    )}
                    label={ () => ( <Text style={{color: 'white'}}>Sign Out</Text>) }
                    onPress={() => {props.navigation.navigate('LogOut')}}
                />
            </Drawer.Section>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      color: 'white',
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      color: 'white',
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      color: 'white',
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        // color: 'white',
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });