import React, { Component } from 'react'
import {
    SafeAreaView,
    ScrollView,
    Switch,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Menu from '../assets/img/menu.png';
import { Avatar, Input, Button } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import ImagePicker from 'react-native-image-crop-picker';

export default class EditProfile extends Component {

    constructor(props) {
        super(props)
        this.takePhotoFromCamera=this.takePhotoFromCamera.bind(this);
    }

    takePhotoFromCamera = async () => {ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        console.log(image);
      });
      }

    render() {
        return (
            <SafeAreaView >
                <View style={{ marginBottom: 50 }}>
                    {/* <View style={styles.head}>
                        <Image style={styles.image} source={Menu} />
                        <Text style={styles.headText}>Edit Profile</Text>
                    </View> */}
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.listView}>
                        <Text style={styles.textHead}>Edit Profile</Text>
                            <Avatar
                                size={100}
                                rounded
                                source={{
                                    uri:
                                        'https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg',
                                }}
                                onAccessoryPress={() => Alert.alert("change avatar")}
                            >
                                <Avatar.Accessory size={24} onPress={this.takePhotoFromCamera} />
                            </Avatar>

                            <Text style={styles.text}>Change Profile Picture</Text>
                            
                            <Input
                                label='First Name'
                                placeholder='User'
                            />
                            <Input
                                label='Middle Name'
                                placeholder='Middle Name'
                            />
                            <Input
                                label='Last Name'
                                placeholder='Last'
                            />
                            <Input
                                label='Mobile Number'
                                placeholder='1234567890'
                                keyboardType='numeric'
                                maxLength={12}
                            />
                            <Input
                                label='Email-Id'
                                placeholder='test@gmail.com'
                            />
                            <View style={{ flexDirection: 'row', width: wp('45%'), alignSelf: 'flex-start' }}>
                                <Input
                                    label='House/apt Number'
                                    placeholder='House/apt Number'
                                />
                                <Input
                                    label='Street Name'
                                    placeholder='Street Name'
                                />
                            </View>
                            <View style={{ flexDirection: 'row', width: wp('45%'), alignSelf: 'flex-start' }}>
                                <Input
                                    label='City'
                                    placeholder='Enter City'
                                />
                                <Input
                                    label='Province'
                                    placeholder='Enter Provience'
                                />
                            </View>
                            <View style={{ flexDirection: 'row', width: wp('30%'), alignSelf: 'flex-start' }}>
                                <Input
                                    label='Post Code'
                                    placeholder='Post Code'
                                />
                                <Input
                                    label='Code'
                                    placeholder='Code'
                                />
                                <Input
                                    label='Country'
                                    placeholder='Enter Contry'
                                />
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginLeft: 20 }}>
                            <Ripple rippleColor="#99FF99" rippleOpacity={0.9} style={styles.loginBtn}>
                                <Text style={styles.loginText}>Save</Text>
                            </Ripple>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    textHead:{
        color: '#00a64f',
        padding: 5,
        fontSize: 23,
        fontWeight: 'bold',
    },
    text: {
        padding: 10,
        color: '#808080',
    },
    listView: {
        alignContent: 'center',
        alignItems: 'center',
        padding: 10
        
    },
    head: {
        backgroundColor: '#1c1c1c',
        flexDirection: 'row',
        textAlign: 'center'
    },
    image: {
        width: wp('10%'),
        height: hp('5%'),
        marginLeft: 20,
    },
    headText: {
        color: 'white',
        marginTop: 10,
        marginLeft: 90,
        fontSize: 20
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('90%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },

});